﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


[System.Serializable]
public class Level
{
    public string nameLevel;
    private int numberStars;
    public Button buttonLevel;
   
}




public class LevelManager : MonoBehaviour
{
    
    public Level[] levels;
    // Start is called before the first frame update
    void Start()
    {
        CheckLevels();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void CheckLevels()
    {
        //PlayerPrefs.DeleteAll();
        int levelReached = PlayerPrefs.GetInt("LevelReached", 1);
        //Si i+1 car le niveau 1 est toujours unlock est supérieur au int LevelReached donc au nombre de niveau fait les boutons des niveaux ne sont pas intéractable
        for (int i = 0; i < levels.Length; i++)
        {
            if (i+1> levelReached)
            {
                levels[i].buttonLevel.interactable = false;
            }
        }
    }

    public void LevelSelection(string nameScene)
    {
        SceneManager.LoadScene(nameScene, LoadSceneMode.Single);
    }

    public void ReturnMenu(int indexMenuScene)
    {
        SceneManager.LoadScene(indexMenuScene, LoadSceneMode.Single);
    }
}
