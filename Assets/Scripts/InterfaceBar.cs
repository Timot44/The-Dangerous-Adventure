﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public interface InterfaceBar
{
    void SetBarMax(int amount);
    void UpdateBar(int amount);
    void DamageBar(int amount);
    void GainBar(int amount);
}