using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour
{
	[HideInInspector]
	public GameObject controlsMenuUi;
	[HideInInspector]
	public GameObject menu;

	public Animator transition;
	public float transitionTime = 1f;
	public void Restart()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
		Time.timeScale = 1f;
	}

	public void Play(int indexScene)
	{
		StartCoroutine((LoadLevel(indexScene)));
	}

	IEnumerator LoadLevel(int levelIndex)
	{
		//Play animation
		transition.SetTrigger("Start");
		//wait
		yield return new WaitForSeconds(transitionTime);
		//LoadScene
		SceneManager.LoadScene(levelIndex, LoadSceneMode.Single);
		Time.timeScale = 1f;
	}

	public void Quit()
	{
		Application.Quit();
	}

	public void Controls()
	{
		menu.SetActive(false);
		controlsMenuUi.SetActive(true);
	}

	public void QuitControls()
	{
		menu.SetActive(true);
		controlsMenuUi.SetActive(false);
	}


}
