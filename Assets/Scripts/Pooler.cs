﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pooler : MonoBehaviour
{
    //Ceci est la class pool qui contient les infos de l'objet qui va etre envoyer dans la queue
    [System.Serializable]
    public class Pool
    {
        public string tag;
        public GameObject prefab;
        public int size;
    }

    public List<Pool> pools;
    public Dictionary<string, Queue<GameObject>> poolDictionary;

    #region Singleton

    public static Pooler Instance;

    private void Awake()
    {
        Instance = this;
    }

    #endregion


    void Start()
    {
        poolDictionary = new Dictionary<string, Queue<GameObject>>();
        //Pour chaque pool on créer une queue d'object et la remplire par rapport a la taille de la pool et pour finir on l'ajoute a notre dictionnaire
        foreach (Pool pool in pools)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();

            for (int i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.prefab);
                obj.SetActive(false);
                objectPool.Enqueue(obj);
            }

            poolDictionary.Add(pool.tag, objectPool);
        }
    }

    //On prend le premier object d'un certain tag de notre dictionnaire et on le retire de la queue pour pouvoir l'instancier 
    public GameObject SpawnFromPool(string tag, Vector3 position, Quaternion rotation)
    {
        if (!poolDictionary.ContainsKey(tag))
        {
            Debug.LogError("No object from this tag to spawn from poolDic ERROR");
            return null;
        }

        GameObject objectToSpawn = poolDictionary[tag].Dequeue();
        objectToSpawn.SetActive(true);

        objectToSpawn.transform.position = position;
        objectToSpawn.transform.rotation = rotation;
        //On check si notre object pooled possede l'interface IpooledManaAI
        PooledMana pooledManaAi = objectToSpawn.GetComponent<PooledMana>();

        if (pooledManaAi != null)
        {
            pooledManaAi.OnObjectSpawn();
        }

        poolDictionary[tag].Enqueue(objectToSpawn);
        return objectToSpawn;
    }
}