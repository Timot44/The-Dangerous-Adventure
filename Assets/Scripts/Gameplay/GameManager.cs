﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public SpawnManager spawnManager;
    public UiManager uiManager;

    [Header("Vagues paramètres")] public int currentWave;
    public int totalWaves;
    public int startingNumberEnemyWave;


    public float timerToNextWave;
    public float maxTimerToNextWave = 1f;
    public bool isWaveReady;

    public static GameManager Instance;
    public GameObject uiParent;
    [Header("Death paramètres")] public GameObject uiGameOver;

    public bool isDie;

    public float maxTimer = 3f;
    public TextMeshProUGUI timerText;
    private float timer;
    [SerializeField] private string sceneToLoad;

    [Header("LevelManager paramètres")] public string nextLevelToLoad;

    public int nextLevelToUnlock;

    [Header("Pause paramètres")] public static bool GameIsPaused;

    public GameObject pauseMenuUi;

    public GameObject optionsMenuUi;

    [Header("Win paramètres")] public GameObject winPanel;
    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
        timerToNextWave = maxTimerToNextWave;
        timer = maxTimer;
        GameIsPaused = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isWaveReady)
        {
            timerToNextWave -= Time.deltaTime;
            Mathf.Clamp(timerToNextWave, 0, Mathf.Infinity);
        }

        if (timerToNextWave <= 0 && !isWaveReady)
        {
            timerToNextWave = maxTimerToNextWave;
            isWaveReady = true;
            spawnManager.SpawnEnemy();
        }

        if (currentWave == totalWaves && SpawnManager.Instance.totalCurrentEnemy <= 0)
        {
            WinLevel();
        }
    }

    void WinLevel()
    {
        //The player win the Level//TODO Faire anim changement de scene + rewards etc...
        winPanel.SetActive(true);
        Time.timeScale = 0f;
    }

    public void ButtonToLevelMenu()
    {
        Time.timeScale = 1f;
        PlayerPrefs.SetInt("LevelReached", nextLevelToUnlock);
        SceneManager.LoadScene(nextLevelToLoad, LoadSceneMode.Single);
    }

    public void PlayerDie()
    {
        uiParent.SetActive(false);
        uiGameOver.SetActive(true);
        timer -= Time.deltaTime;
        timerText.text = $"Restart in {timer:0}s";
        if (timer <= 0)
        {
            uiParent.SetActive(true);
            uiGameOver.SetActive(false);
            isDie = false;
            timer = maxTimer;
            SceneManager.LoadScene(sceneToLoad, LoadSceneMode.Single);
        }
    }

    public void OnPauseButton()
    {
        if (GameIsPaused)
        {
            Resume();
        }
        else
        {
            Pause();
        }
    }

    void Resume()
    {
        pauseMenuUi.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }

    void Pause()
    {
        pauseMenuUi.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    public void Options()
    {
        if (pauseMenuUi.activeSelf)
        {
            pauseMenuUi.SetActive(false);
            optionsMenuUi.SetActive(true);
        }
        else
        {
            optionsMenuUi.SetActive(false);
            pauseMenuUi.SetActive(true);
        }
    }

    public void LoadMenu(int indexMenuScene)
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(indexMenuScene, LoadSceneMode.Single);
    }
}