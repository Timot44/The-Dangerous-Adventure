﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Wave", menuName = "ScriptableObject/WaveObject", order =1 )]
public class Wave : ScriptableObject
{
   
    [Header("Stamina Enemy")] public GameObject staminaEnemy;
    public int numberOfStaminaEnemy;
    [Header("Mana Enemy")] public GameObject manaEnemy;
    public int numberOfManaEnemy;
    [Header("Health Enemy")] public GameObject healthEnemy;
    public int numberOfHealthEnemy;
}
