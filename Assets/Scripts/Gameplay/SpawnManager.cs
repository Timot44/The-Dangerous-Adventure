﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class SpawnManager : MonoBehaviour
{
    

    public Vector2 screenBounds;
    public float objectHeigth;
    public float objectWidth;
    

    public int totalCurrentEnemy;
    [SerializeField] private Transform player;
    [SerializeField] private Slider[] playerBars;

    [SerializeField] private Transform moveSpotManager;

    public Pooler manaPooler;

    public static SpawnManager Instance;

    public List<Wave> waves;

  
    // Start is called before the first frame update
    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        screenBounds =
            Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void SpawnEnemy()
    {
        if (moveSpotManager.childCount > 0)
        {
            foreach (Transform child in moveSpotManager)
            {
                Destroy(child.gameObject);
            }
        }

        

        EnemyInstantiate(waves[GameManager.Instance.currentWave].numberOfManaEnemy, waves[GameManager.Instance.currentWave].manaEnemy, playerBars[0]);
        EnemyInstantiate(waves[GameManager.Instance.currentWave].numberOfHealthEnemy, waves[GameManager.Instance.currentWave].healthEnemy, playerBars[1]);
        EnemyInstantiate(waves[GameManager.Instance.currentWave].numberOfStaminaEnemy, waves[GameManager.Instance.currentWave].staminaEnemy, playerBars[2]);

       
        GameManager.Instance.startingNumberEnemyWave = totalCurrentEnemy;
        
        GameManager.Instance.currentWave++;
        UiManager.Instance.TextSetup(GameManager.Instance.currentWave, GameManager.Instance.totalWaves,
            totalCurrentEnemy, UiManager.Instance.waveText, UiManager.Instance.currentEnemyText);
    }

    public void EnemyInstantiate(int number, GameObject prefab, Slider playerBar)
    {
        objectWidth = prefab.GetComponent<SpriteRenderer>().bounds.extents.x;
        objectHeigth = prefab.GetComponent<SpriteRenderer>().bounds.extents.y;
        for (int i = 0; i < number; i++)
        {
            Vector2 enemySpawnPos =
                new Vector2(Random.Range(-screenBounds.x + objectWidth, screenBounds.x - objectWidth),
                    Random.Range(screenBounds.y + objectHeigth, screenBounds.y - objectHeigth));

            GameObject moveSpots = new GameObject(prefab.name + "MoveSpot");

            moveSpots.transform.position = enemySpawnPos;
            moveSpots.transform.SetParent(moveSpotManager);

            prefab.GetComponent<EnemyAi>().playerTarget = player;
            prefab.GetComponent<EnemyAi>().barPlayer = playerBar;
            prefab.GetComponent<EnemyAi>().moveSpot = moveSpots.transform;

            if (prefab.GetComponentInChildren<AIManaStartAttack>())
            {
                prefab.GetComponentInChildren<AIManaStartAttack>().enemyManaPooler = manaPooler;
            }

            GameObject currentEnemy = Instantiate(prefab, enemySpawnPos, Quaternion.identity);
            currentEnemy.name = prefab.name + i;
           
            totalCurrentEnemy++;
        }
    }
}