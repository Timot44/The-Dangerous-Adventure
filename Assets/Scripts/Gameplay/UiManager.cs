﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UiManager : MonoBehaviour
{
    public TextMeshProUGUI waveText;

    public TextMeshProUGUI currentEnemyText;


    public static UiManager Instance;
    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
    }

    public void TextSetup(int currentWave, int maxWaves, int currentEnemyWave, TextMeshProUGUI waveText,
        TextMeshProUGUI currentEnemyText)
    {
        waveText.text = $"Waves : {currentWave} / {maxWaves}";
        currentEnemyText.text = $"Enemy : {currentEnemyWave}";
    }

    public void EnemyTextUpdate(int currentEnemy)
    {
        currentEnemyText.text = $"Enemy : {currentEnemy}";
    }
}