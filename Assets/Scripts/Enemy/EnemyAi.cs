﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using DG.Tweening;

public abstract class EnemyAi : MonoBehaviour
{
    [Header("Base Parameters")] public string name;
    public int healthPoint;
    public int damageToPlayer;
    public int amountBarRestored;
    public Slider barPlayer;
    public float objectWidth;
    public float objectHeight;
    public GameObject deathVfxParticle;
    public GameObject attackVfxParticle;
    
    [Header("Patrol Attributes")] public float patrolSpeed = 5f;
    public Transform moveSpot;
    public float waitTime;
    public float startWaitTime = 3f;
    public Vector2 screenBounds;
    public float patrolDistanceToChasing;
    [Header("Chasing Player Attributes")] public Transform playerTarget;
    public float chaseSpeed;
    public float chasingDistanceToAttack;
    public float maxDistanceToPatrol;
    [Header("Attack Player Attributes ")] public float attackDistance;
    public float currentAttackTimer;
    public float maxAttackTimer;
    public Vector3 posCloseToPlayer;
    [Range(0f, 4f)] public float radiusDistCloseToPlayer;
    
    protected AIState currentState;
    [Header("STAMINA ENEMY STATE")]
    public AIStaminaStartAttack AIStaminaStartAttack = new AIStaminaStartAttack();
    public AIStaminaStartChasing AIStaminaStartChasing = new AIStaminaStartChasing();
    public AIStaminaStartPatrol AIStaminaStartPatrol = new AIStaminaStartPatrol();
    
    [Header("MANA ENEMY STATE")]
    public AIManaStartAttack AIManaStartAttack = new AIManaStartAttack();
    public AIManaStartChasing AIManaStartChasing = new AIManaStartChasing();
    public AIManaStartPatrol AIManaStartPatrol = new AIManaStartPatrol();
    
    [Header("HEALTH ENEMY STATE")]
    public AIHealthStartAttack AIHealthStartAttack = new AIHealthStartAttack();
    public AIHealthStartChasing AIHealthStartChasing = new AIHealthStartChasing();
    public AIHealthStartPatrol AIHealthStartPatrol = new AIHealthStartPatrol();
    public EnemyAi(string name, int healthPoint, int damageToPlayer, int amountBarRestored, Slider barPlayer)
    {
        this.name = name;
        this.healthPoint = healthPoint;
        this.damageToPlayer = damageToPlayer;
        this.amountBarRestored = amountBarRestored;
        this.barPlayer = barPlayer;
    }


    public Vector3 PosAttackClosePlayer(EnemyAi enemyAi)
    {
        //On prend la position du player
        Vector3 posPlayer = enemyAi.playerTarget.position;
        //On prend un point qui est à l'intérieur d'un cercle d"un "" de radius autour du player
        Vector3 posClosToPlayer = (Vector2) posPlayer + (Random.insideUnitCircle * enemyAi.radiusDistCloseToPlayer);

        return posClosToPlayer;
    }


    public void TakeDamage(int amount)
    {
        healthPoint -= amount;
        barPlayer.gameObject.GetComponent<InterfaceBar>().GainBar(amountBarRestored);

        if (healthPoint <= 0)
        {
            SpawnManager.Instance.totalCurrentEnemy--;
            UiManager.Instance.EnemyTextUpdate(SpawnManager.Instance.totalCurrentEnemy);
            Camera.main.DOShakePosition(0.7f, 0.3f, 8, 180f);
            GameObject deathParticle = Instantiate(deathVfxParticle, transform.position, Quaternion.identity);
            Destroy(deathParticle, 2f);
            Destroy(gameObject);

            if (SpawnManager.Instance.totalCurrentEnemy <= 0)
            {
                GameManager.Instance.isWaveReady = false;
            }
        }
    }


    public void SwitchState(AIState state)
    {
        currentState = state;
        state.EnterState(this);
    }


   /* public void DrawDebugSphere(Vector2 center, float radius) => Handles.DrawWireDisc(center, Vector3.forward, radius);*/
}