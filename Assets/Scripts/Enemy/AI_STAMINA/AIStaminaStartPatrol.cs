﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIStaminaStartPatrol : AIState
{
    



   


    public override void EnterState(EnemyAi enemyAi)
    {
       enemyAi.objectWidth = enemyAi.transform.GetComponent<SpriteRenderer>().bounds.extents.x;
        enemyAi.objectWidth = enemyAi.transform.GetComponent<SpriteRenderer>().bounds.extents.y;
      enemyAi.waitTime = enemyAi.startWaitTime;
        enemyAi.screenBounds =
            Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
       enemyAi.moveSpot.position = new Vector2(Random.Range(-enemyAi.screenBounds.x + enemyAi.objectWidth, enemyAi.screenBounds.x - enemyAi.objectWidth),
            Random.Range(enemyAi.screenBounds.y + enemyAi.objectHeight, enemyAi.screenBounds.y - enemyAi.objectHeight));

        enemyAi.currentAttackTimer = enemyAi.maxAttackTimer;
    }

    public override void UpdateState(EnemyAi enemyAi)
    {
        enemyAi.waitTime -= Time.deltaTime;


        enemyAi.transform.position = Vector2.MoveTowards(enemyAi.transform.position, enemyAi.moveSpot.position,
            enemyAi.patrolSpeed * Time.deltaTime);

        if (Vector2.Distance(enemyAi.transform.position, enemyAi.moveSpot.position) < 0.2f)
        {
            if (enemyAi.waitTime <= 0)
            {
                //Si le l'ennemi arrive a terme de son waitTime il change de moveposition
                enemyAi.moveSpot.position = new Vector2(Random.Range(-enemyAi.screenBounds.x, enemyAi.screenBounds.x),
                    Random.Range(-enemyAi.screenBounds.y, enemyAi.screenBounds.y));
                enemyAi.waitTime = enemyAi.startWaitTime;
            }
        }

        //Si la distance de l'enemy est asser proche il passe en mode chasing
        if (Vector2.Distance(enemyAi.transform.position, enemyAi.playerTarget.position) <
            enemyAi.patrolDistanceToChasing)
        {
            enemyAi.SwitchState(enemyAi.AIStaminaStartChasing);
        }
    }

 /*  public override AIState RunCurrentState(EnemyAi enemyAi1)
    {
        throw new System.NotImplementedException();
    }*/


    /* private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.position, 2.5f);
    }*/
}