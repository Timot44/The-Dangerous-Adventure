﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaminaEnemy : EnemyAi
{
   
    // Start is called before the first frame update
    public StaminaEnemy(string name, int healthPoint, int damageToPlayer, int amountBarRestored, Slider barPlayer) :
        base(name, healthPoint, damageToPlayer, amountBarRestored, barPlayer)
    {
    }

 /*   private void RunStateMachine(EnemyAi enemyAi)
    {
        //Check si la currente state n'appele pas RuncurrentState
        AIState nexState = currentState?.RunCurrentState(enemyAi);

        if (nexState != null)
        {
            //Switch to nexstate
            SwitchState(nexState);
        }
    }*/

   


    public void Start()
    {
        currentState = AIStaminaStartPatrol;
        currentState.EnterState(this);
      
    }

    // Update is called once per frame
    void Update()
    {
        currentState.UpdateState(this);
    }
}