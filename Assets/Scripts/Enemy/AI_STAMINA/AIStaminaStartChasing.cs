﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIStaminaStartChasing : AIState
{
    


    public override void EnterState(EnemyAi enemyAi)
    {
        
    }

    public override void UpdateState(EnemyAi enemyAi)
    {
        //Si l'enemy est suffisament proche du player il va vers lui 
        enemyAi.transform.position = Vector2.MoveTowards(enemyAi.transform.position, enemyAi.playerTarget.position,
            enemyAi.chaseSpeed * Time.deltaTime);
        //Si il s'éloigne trop il retourne en mode patrouille
        if (Vector2.Distance(enemyAi.transform.position, enemyAi.playerTarget.position) > enemyAi.maxDistanceToPatrol)
        {
           enemyAi.SwitchState(enemyAi.AIStaminaStartPatrol);
        }


        if (Vector2.Distance(enemyAi.transform.position, enemyAi.playerTarget.position) <=
            enemyAi.chasingDistanceToAttack)
        {
            //Si l'enemy est a porter d'attaquer il passe en mode attaque
            // Debug.Log(enemyAi + "va attaquer le joueur");
            enemyAi.posCloseToPlayer = enemyAi.PosAttackClosePlayer(enemyAi);
            enemyAi.SwitchState(enemyAi.AiStaminaStartAttack);
        }

        
    }

    /*public override AIState RunCurrentState(EnemyAi enemyAi)
    {
       
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, 1.5f);
    }*/
}