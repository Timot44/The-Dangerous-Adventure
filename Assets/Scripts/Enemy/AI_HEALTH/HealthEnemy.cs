﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthEnemy : EnemyAi
{
    public AIState currentState;

    // Start is called before the first frame update
   
  /*  private void RunStateMachine(EnemyAi enemyAi)
    {
        //Check si la currente state n'appele pas RuncurrentState
        AIState nexState = currentState?.RunCurrentState(enemyAi);

        if (nexState != null)
        {
            //Switch to nexstate
            NextState(nexState);
        }
    }*/

  

    // Update is called once per frame
    public void Start()
    {
        currentState = AIStaminaStartPatrol;
        currentState.EnterState(this);
      
    }

    // Update is called once per frame
    void Update()
    {
        currentState.UpdateState(this);
    }

    public HealthEnemy(string name, int healthPoint, int damageToPlayer, int amountBarRestored, Slider barPlayer) : base(name, healthPoint, damageToPlayer, amountBarRestored, barPlayer)
    {
    }
}
