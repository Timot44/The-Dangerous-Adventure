﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AIState
{
 


 public abstract void EnterState(EnemyAi enemyAi);

 public abstract void UpdateState(EnemyAi enemyAi);

 //public abstract void Attack(EnemyAi enemyAi);

// public abstract AIState RunCurrentState(EnemyAi enemyAi1);

}
