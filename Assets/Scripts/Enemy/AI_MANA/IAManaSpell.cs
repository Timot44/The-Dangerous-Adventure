﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAManaSpell : MonoBehaviour, PooledMana
{

    public float spellSpeed = 3f;
    [SerializeField]
    private Transform player;
    [SerializeField]
    private Vector2 target;
    [SerializeField]
    private float radiusColl = 0.1f;

    

    public int damageToPlayer;
    public void OnObjectSpawn()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        Vector3 playerpos = player.transform.position;
        target = new Vector2(playerpos.x, playerpos.y);
        
    }

    private void Update()
    {
        //Déplacement du projectile
        gameObject.transform.position = Vector2.MoveTowards(gameObject.transform.position, target, spellSpeed * Time.deltaTime);

       
        if (Vector2.Distance(gameObject.transform.position, player.transform.position) <= radiusColl)
        {
            HealthBar._instance.DamageBar(damageToPlayer);
            gameObject.SetActive(false);
        }
        else if (Vector2.Distance(gameObject.transform.position, target) <= radiusColl)
        {
            gameObject.SetActive(false);
        }
    }
}