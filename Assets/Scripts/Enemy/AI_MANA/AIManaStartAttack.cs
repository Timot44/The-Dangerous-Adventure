﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIManaStartAttack : AIState
{
   
    public Pooler enemyManaPooler;
   
    public void Attack(EnemyAi enemyAi)
    {
        enemyManaPooler.SpawnFromPool("IAManaSpell", enemyAi.transform.position, Quaternion.identity);
    }

 
   /* private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, 2f);
    }*/
   public override void EnterState(EnemyAi enemyAi)
   {
       throw new System.NotImplementedException();
   }

   public override void UpdateState(EnemyAi enemyAi)
   {
       //L'ennemy va en direction du joueur
       enemyAi.transform.position = Vector2.MoveTowards(enemyAi.transform.position, enemyAi.posCloseToPlayer , enemyAi.chaseSpeed * Time.deltaTime);
       enemyAi.currentAttackTimer -= Time.deltaTime;

       enemyAi.currentAttackTimer = Mathf.Clamp(enemyAi.currentAttackTimer, 0, Mathf.Infinity);
       //Si il s'éloigne trop il retourne en mode chasing
       if (Vector2.Distance(enemyAi.transform.position, enemyAi.playerTarget.position) > enemyAi.attackDistance)
       {
           enemyAi.SwitchState(enemyAi.AIManaStartChasing);
       }
       // Si il est asser proche il peut attaquer le joueur
       if (Vector2.Distance(enemyAi.transform.position, enemyAi.playerTarget.position) <= enemyAi.attackDistance)
       {
          
            
           if (enemyAi.currentAttackTimer <= 0)
           {
               Attack(enemyAi);
               enemyAi.currentAttackTimer = enemyAi.maxAttackTimer;
               
           }
           
       }
   }
}
