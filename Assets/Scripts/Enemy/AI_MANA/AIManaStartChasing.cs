﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIManaStartChasing : AIState
{

   /* public override AIState RunCurrentState(EnemyAi enemyAi)
    {
        //Si l'enemy est suffisament proche du player il va vers lui 
        enemyAi.transform.position = Vector2.MoveTowards(enemyAi.transform.position, enemyAi.playerTarget.position,
            enemyAi.chaseSpeed * Time.deltaTime);
        //Si il s'éloigne trop il retourne en mode patrouille
        if (Vector2.Distance(enemyAi.transform.position, enemyAi.playerTarget.position) > enemyAi.maxDistanceToPatrol)
        {
            return startPatrol;
        }


        if (Vector2.Distance(enemyAi.transform.position, enemyAi.playerTarget.position) <=
            enemyAi.chasingDistanceToAttack)
        {
            //Si l'enemy est a porter d'attaquer il passe en mode attaque
            //    Debug.Log(enemyAi + "va attaquer le joueur");
            enemyAi.posCloseToPlayer = enemyAi.PosAttackClosePlayer(enemyAi);
            return startAttack;
        }


        return this;
    }*/

   /* private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, 2.5f);
    }*/
   public override void EnterState(EnemyAi enemyAi)
   {
       throw new System.NotImplementedException();
   }

   public override void UpdateState(EnemyAi enemyAi)
   {
       //Si l'enemy est suffisament proche du player il va vers lui 
       enemyAi.transform.position = Vector2.MoveTowards(enemyAi.transform.position, enemyAi.playerTarget.position,
           enemyAi.chaseSpeed * Time.deltaTime);
       //Si il s'éloigne trop il retourne en mode patrouille
       if (Vector2.Distance(enemyAi.transform.position, enemyAi.playerTarget.position) > enemyAi.maxDistanceToPatrol)
       {
           enemyAi.SwitchState(enemyAi.AIManaStartPatrol);
       }


       if (Vector2.Distance(enemyAi.transform.position, enemyAi.playerTarget.position) <=
           enemyAi.chasingDistanceToAttack)
       {
           //Si l'enemy est a porter d'attaquer il passe en mode attaque
           //    Debug.Log(enemyAi + "va attaquer le joueur");
           enemyAi.posCloseToPlayer = enemyAi.PosAttackClosePlayer(enemyAi);
           enemyAi.SwitchState(enemyAi.AIManaStartAttack);
       }


       
   }
}