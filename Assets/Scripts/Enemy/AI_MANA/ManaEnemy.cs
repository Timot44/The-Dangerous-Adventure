﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManaEnemy : EnemyAi
{
    public AIState currentState;
  
    
    public ManaEnemy(string name, int healthPoint, int damageToPlayer, int amountBarRestored, Slider barPlayer) : base(name, healthPoint, damageToPlayer, amountBarRestored, barPlayer)
    {
    }
    /*private void RunStateMachine(EnemyAi enemyAi)
    {
        //Check si la currente state n'appele pas RuncurrentState
        AIState nexState = currentState?.RunCurrentState(enemyAi);
        
        if (nexState != null)
        {
            //Switch to nexstate
            NextState(nexState);
        }
    }*/

    public void Start()
    {
        currentState = AIStaminaStartPatrol;
        currentState.EnterState(this);
      
    }

    // Update is called once per frame
    void Update()
    {
        currentState.UpdateState(this);
    }
}
