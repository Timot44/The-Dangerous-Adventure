﻿
using UnityEditor;
using UnityEngine;

public class PlayersAttack : MonoBehaviour
{
    private Attacks attacks = new Attacks();

    [Range(0, 100)] public int playerDamage;

    [Header("Bars")] public ManaBar manaBar;
    public StaminaBar staminaBar;
    public HealthBar healthBar;

    [Header("Stamina Attack Interface")] [Range(0f, 4f)]
    public float radiusStaminaAttack;

    public ParticleSystem vfxStaminaAttackEffect;
    [Range(0f, 4f)] public float distStaminaAttack;

    public LayerMask layers;

    [Header("Mana Attack Interface")] [Range(0f, 4f)]
    public float radiusManaAttack;

    [Range(0f, 4f)] public float distManaAttack;
    public Pooler playerManaAttack;


    [Header("Health Attack Interface")] [Range(0f, 4f)]
    public float radiusHealthAttack;
    
    [Range(0f, 4f)] public float distHealthAttack;

    public ParticleSystem vfxHealthAttackEffect;
    //private Transform enemy;
    [SerializeField] private Vector2 target;
    [Range(0f, 2f)]
    [SerializeField] private float radiusKnockback;

    public enum PlayerAttacks
    {
        Stamina,
        Health
    }
    // Start is called before the first frame update
    void Start()
    {
    }

    /*private void OnDrawGizmos()
    {
        Handles.color = Color.green;
        Handles.DrawWireDisc(gameObject.transform.position, Vector3.forward, radiusStaminaAttack);
        Handles.color = Color.blue;
        Handles.DrawWireDisc(gameObject.transform.position, Vector3.forward, radiusManaAttack);
        Handles.color = Color.red;
        Handles.DrawWireDisc(gameObject.transform.position, Vector3.forward, radiusHealthAttack);
    }*/

    public void ManaInterface()
    {
        attacks.ManaAttack();
      

        if (manaBar.currentValue > 0)
        {
            Vector2 playerPos = transform.position;

            RaycastHit2D ray = Physics2D.CircleCast(playerPos, radiusManaAttack, Vector2.up, distManaAttack, layers);
            PlayerAttackMana(ray, radiusManaAttack);
        }
    }

    private void PlayerAttackMana(RaycastHit2D raycastHit, float attackRange)
    {
        float range = attackRange;
        Transform iaObjClosest = new RectTransform();

        Vector3 currentPos = transform.position;
        Vector2 targetPos = new Vector2();

        float minDist = 1000;

        // si un raycast touche on va mettre le gameobject hit dans un d'object
        if (raycastHit)
        {
            //On récupère le gameobject caster par le Raycasthit
            Vector3 directionToTarget = raycastHit.collider.transform.position - currentPos;
            float distSqrToTarget = directionToTarget.sqrMagnitude;
            manaBar.DamageBar(10);
            if (distSqrToTarget <= minDist && distSqrToTarget <= range)
            {
                minDist = distSqrToTarget;
                iaObjClosest = raycastHit.collider.transform;

                Vector3 posEnemy = iaObjClosest.position;
                targetPos = new Vector2(posEnemy.x, posEnemy.y);

                GameObject manaProjectile = playerManaAttack.SpawnFromPool("PlayerManaSpell",
                    gameObject.transform.position, Quaternion.identity);

                manaProjectile.GetComponent<PlayerManaSpell>().enemy = iaObjClosest;
                manaProjectile.GetComponent<PlayerManaSpell>().target = targetPos;
                manaProjectile.GetComponent<PlayerManaSpell>().playerDamage = playerDamage;
            }
        }
    }

    public void StaminaInterface()
    {
        attacks.StaminaAttack();
        
        if (staminaBar.currentValue > 0)
        {
            Vector2 playerPos = transform.position;
            RaycastHit2D ray =
                Physics2D.CircleCast(playerPos, radiusStaminaAttack, Vector2.up, distStaminaAttack, layers);
            PlayerAttackCAC(ray, radiusStaminaAttack, playerDamage, PlayerAttacks.Stamina);
        }
    }

    private void PlayerAttackCAC(RaycastHit2D raycastHit, float attackRange, int damage, PlayerAttacks playerAttacktype)
    {
        float range = attackRange;
        GameObject iaObjClosest;
        Vector3 currentPos = transform.position;

        float minDist = 1000;


        if (raycastHit)
        {
            //On récupère le gameobject toucher par le Raycasthit
            Vector3 directionToTarget = raycastHit.collider.transform.position - currentPos;
            float distSqrToTarget = directionToTarget.sqrMagnitude;
         
            if (distSqrToTarget <= minDist && distSqrToTarget <= range)
            {
                minDist = distSqrToTarget;
                iaObjClosest = raycastHit.collider.gameObject;
                Debug.Log(iaObjClosest.name);

                iaObjClosest.GetComponent<EnemyAi>().TakeDamage(damage);
                if (playerAttacktype == PlayerAttacks.Health)
                {
                    vfxHealthAttackEffect.Play();
                    healthBar.DamageBar(10);
                }
                else if (playerAttacktype == PlayerAttacks.Stamina)
                {
                    vfxStaminaAttackEffect.Play();
                    staminaBar.DamageBar(10);

                }
               
                Vector3 knockback = (iaObjClosest.transform.position - transform.position).normalized * radiusKnockback;
                iaObjClosest.transform.position = new Vector3(iaObjClosest.transform.position.x + knockback.x, iaObjClosest.transform.position.y + knockback.y, iaObjClosest.transform.position.z + knockback.z );
            }
        }
    }

    public void HealthInterface()
    {
        attacks.HealthAttack();
        
        if (healthBar.currentValue > 0)
        {
            Vector2 playerPos = transform.position;
            RaycastHit2D ray =
                Physics2D.CircleCast(playerPos, radiusStaminaAttack, Vector2.up, distHealthAttack, layers);
            PlayerAttackCAC(ray, radiusHealthAttack, playerDamage, PlayerAttacks.Health);
        }
    }


    // Update is called once per frame
    void Update()
    {
    }
}

public interface IAttack
{
    void ManaAttack();
    void StaminaAttack();
    void HealthAttack();
}

public class Attacks : IAttack
{
    public void ManaAttack()
    {
    }

    public void StaminaAttack()
    {
    }


    public void HealthAttack()
    {
    }
}