﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManaSpell : MonoBehaviour
{
    public float spellSpeed = 3f;
    [SerializeField]
    public Transform enemy;
    [SerializeField]
    public Vector2 target;
    [SerializeField]
    private float radiusColl = 0.1f;

    public int playerDamage;

    public GameObject vfxManaPlayerAttack;
    // Update is called once per frame
    void Update()
    {
        //Déplacement du projectile
        gameObject.transform.position = Vector2.MoveTowards(gameObject.transform.position, target, spellSpeed * Time.deltaTime);

        if (enemy)
        {
            if (Vector2.Distance(gameObject.transform.position, enemy.transform.position) <= radiusColl)
            {
                enemy.GetComponent<EnemyAi>().TakeDamage(playerDamage);
                GameObject vfxShoot =  Instantiate(vfxManaPlayerAttack, transform.position, Quaternion.identity);
                Destroy(vfxShoot, 2f);
              /*  Vector3 knockback = (enemy.transform.position - transform.position).normalized * 0.2f;
                enemy.transform.position = new Vector3(enemy.transform.position.x + knockback.x, enemy.transform.position.y + knockback.y, enemy.transform.position.z + knockback.z );*/
                gameObject.SetActive(false);
            }
           
        }
        if (Vector2.Distance(gameObject.transform.position, target) <= radiusColl)
        {
            gameObject.SetActive(false);
        }
       
    }
}
