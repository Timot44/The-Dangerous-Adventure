﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public Joystick joystick;
    
    public float speed= 5f;
    private Rigidbody2D _rigidbody2D;
    private Vector2 _direction;

    private Vector2 screenBounds;
    private float objectWidth;
    private float objectHeight;
    private void Start()
    {
        _rigidbody2D = gameObject.GetComponent<Rigidbody2D>();
        //On récupère la taille de notre écran 
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        // On récupère la taille de notre objet
        objectWidth = transform.GetComponent<SpriteRenderer>().bounds.extents.x;
        objectHeight = transform.GetComponent<SpriteRenderer>().bounds.extents.y;
      
    }

    void Update()
    {
         _direction = joystick.Direction;
       
    }

    private void FixedUpdate()
    {
        _rigidbody2D.MovePosition(_rigidbody2D.position + _direction * (speed * Time.fixedDeltaTime));
    }

    private void LateUpdate()
    {
        Vector3 viewPos = transform.position;
        viewPos.x = Mathf.Clamp(viewPos.x, screenBounds.x * -1 + objectWidth, screenBounds.x - objectWidth);
        viewPos.y = Mathf.Clamp(viewPos.y, screenBounds.y * -1 + objectHeight, screenBounds.y - objectHeight);
        transform.position = viewPos;
    }
}
