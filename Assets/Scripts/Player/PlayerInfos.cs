﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerInfos : MonoBehaviour
{
    [SerializeField]
    private HealthBar playerHealth;

 
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if (playerHealth.currentValue <= 0 && !GameManager.Instance.isDie)
        {
            GameManager.Instance.PlayerDie();
        }
    }

    
}
