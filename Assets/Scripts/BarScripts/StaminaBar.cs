﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaminaBar : MonoBehaviour, InterfaceBar
{
    public Slider slider;
    public int maxValue = 100;
    public int currentValue;
    private Image damagedBarImage;
    private Color damagedColor;
    private float damagedStaminaFadeTimer;
    public float damagedStaminaFadeMaxTimer = 1f;

    private void Awake()
    {
        damagedBarImage = transform.Find("DamageFill").GetComponent<Image>();
        damagedColor = damagedBarImage.color;
        damagedColor.a = 0f;
        damagedBarImage.color = damagedColor;
    }

    private void Start()
    {
        currentValue = maxValue;
        SetBarMax(maxValue);
        damagedBarImage.fillAmount = slider.value / maxValue;
    }

    private void Update()
    {
        if (damagedColor.a > 0)
        {
            damagedStaminaFadeTimer -= Time.deltaTime;
            if (damagedStaminaFadeTimer < 0)
            {
                float fadeAmount = 5f;
                damagedColor.a -= fadeAmount * Time.deltaTime;
                damagedBarImage.color = damagedColor;
            }
        }
    }

    public void SetBarMax(int amount)
    {
        slider.maxValue = amount;
        slider.value = amount;
    }

    public void UpdateBar(int amount)
    {
        slider.value = amount;
    }

    public void DamageBar(int amount)
    {
        currentValue -= amount;
    

     
        if (damagedColor.a <= 0)
        {
            //La barre de dommage est invisible
            damagedBarImage.fillAmount = slider.value / maxValue;
        }
        //La barre de dommage est déjà visible
        damagedColor.a = 1;
        damagedBarImage.color = damagedColor;
        damagedStaminaFadeTimer = damagedStaminaFadeMaxTimer;
        
            
        if (currentValue < 0)
        {
            currentValue = 0;
        }
        UpdateBar(currentValue);
    }

    public void GainBar(int amount)
    {
        currentValue += amount;
   
        
        if (currentValue > maxValue)
        {
            currentValue = maxValue;
        }

        UpdateBar(currentValue);
    }
}